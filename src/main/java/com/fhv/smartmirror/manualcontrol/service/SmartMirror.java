package com.fhv.smartmirror.manualcontrol.service;

import com.fhv.smartmirror.manualcontrol.web.EventEndPoint;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

@Service
public class SmartMirror {
    private final static Logger logger = LoggerFactory.getLogger(SmartMirror.class);

    private final EventEndPoint eventEndPoint;

    public SmartMirror(EventEndPoint eventEndPoint) {
        this.eventEndPoint = eventEndPoint;
    }

    public void reset(Mode mode) {
        logger.info("Reset: {}", mode);
        eventEndPoint.sendEvent("reset",  mode.toString());
    }

    public void addSteps(int count) {
        logger.info("Add steps: {}", count);
        eventEndPoint.sendEvent("steps", String.valueOf(count));
    }

    public int getListenerCount() {
        return eventEndPoint.getListenerCount();
    }

    public enum Mode {
        CHART, TREE
    }
}
