package com.fhv.smartmirror.manualcontrol.web;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.mvc.method.annotation.SseEmitter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@RestController
@CrossOrigin("*")
public class EventEndPoint {
    private static final Logger logger = LoggerFactory.getLogger(EventEndPoint.class);

    private final List<SseEmitter> emitters = new ArrayList<>();

    @GetMapping(value = "events", produces = MediaType.TEXT_EVENT_STREAM_VALUE)
    public SseEmitter events() {
        logger.debug("Add event listener.");
        SseEmitter sseEmitter = new SseEmitter();
        emitters.add(sseEmitter);
        sseEmitter.onCompletion(() -> {
            logger.debug("Removed event listener.");
            emitters.remove(sseEmitter);
        });
        return sseEmitter;
    }

    public void sendEvent(String name, String data) {
        logger.debug("Sending event to {} listeners: {} {}", emitters.size(), name, data);
        emitters.forEach(emitter -> {
            try {
                emitter.send(SseEmitter.event()
                        .name(name)
                        .data(data));
            } catch (IOException e) {
                logger.error("Error sending event.", e);
            }
        });
    }

    public int getListenerCount() {
        return emitters.size();
    }
}
