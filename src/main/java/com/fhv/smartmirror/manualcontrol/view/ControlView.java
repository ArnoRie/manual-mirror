package com.fhv.smartmirror.manualcontrol.view;

import com.fhv.smartmirror.manualcontrol.service.SmartMirror;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import javax.swing.*;
import java.awt.*;

@Component
public class ControlView implements ApplicationRunner {
    private final SmartMirror smartMirror;
    private JLabel countLabel;

    public ControlView(SmartMirror smartMirror) {
        this.smartMirror = smartMirror;
    }

    @Override
    public void run(ApplicationArguments applicationArguments) throws Exception {
        JFrame frame = new JFrame(ControlView.class.getSimpleName());
        frame.setLayout(new GridLayout(3, 2));

        // Reset
        JComboBox<SmartMirror.Mode> resetDropdown = new JComboBox<>(SmartMirror.Mode.values());
        JButton resetButton = new JButton("Reset");
        resetButton.addActionListener(e -> {
            smartMirror.reset((SmartMirror.Mode) resetDropdown.getSelectedItem());
        });

        frame.add(resetDropdown);
        frame.add(resetButton);

        // Steps
        JSpinner stepsInput = new JSpinner();
        JButton stepsButton = new JButton("Add Steps");
        stepsButton.addActionListener(e -> {
            smartMirror.addSteps((Integer) stepsInput.getValue());
        });

        frame.add(stepsInput);
        frame.add(stepsButton);

        // Listener count
        JLabel listenerLabel = new JLabel("Listeners");
        countLabel = new JLabel("-");

        frame.add(listenerLabel);
        frame.add(countLabel);

        frame.pack();
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        frame.setVisible(true);
    }

    @Scheduled(fixedRate = 500L, initialDelay = 5000L)
    public void updateListenerCount() {
        int count = smartMirror.getListenerCount();
        countLabel.setText(String.valueOf(count));
    }
}
