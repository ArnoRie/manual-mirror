package com.fhv.smartmirror.manualcontrol;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class ManualControlApplication {

	public static void main(String[] args) {
        SpringApplication springApplication = new SpringApplication(ManualControlApplication.class);
        springApplication.setHeadless(false);
        springApplication.run(args);
	}
}
