package com.fhv.smartmirror.manualcontrol;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ManualControlApplicationTests {

	@Test
	public void contextLoads() {
	}

}
